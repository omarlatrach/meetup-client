package com.thedevopsnerdworld.meetup;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketMessage;

@Component
@EnableBinding(Source.class)
public class RsvpsKafkaProducer {

	@Value("${sending.message.timeout.ms}")
	private int timeout;

	private final Source source;

	public RsvpsKafkaProducer(Source source) {
		this.source = source;
	}

	public void sendRsvpMessage(WebSocketMessage<?> message) {
		source.output().send(MessageBuilder.withPayload(message.getPayload()).build(), timeout);
	}
}

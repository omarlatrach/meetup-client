package com.thedevopsnerdworld.meetup;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
class RsvpsWebSocketHandler extends AbstractWebSocketHandler {

	private final RsvpsKafkaProducer rsvpsKafkaProducer;

	public RsvpsWebSocketHandler(RsvpsKafkaProducer rsvpsKafkaProducer) {
		this.rsvpsKafkaProducer = rsvpsKafkaProducer;
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) {
		log.info("New RSVP:\n {}", message.getPayload());
		rsvpsKafkaProducer.sendRsvpMessage(message);
	}
}

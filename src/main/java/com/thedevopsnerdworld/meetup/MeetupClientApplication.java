package com.thedevopsnerdworld.meetup;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

@SpringBootApplication
public class MeetupClientApplication {

	@Value("${meetup.rsvps.endpoint}")
	private String meetupRsvpsEndpoint;

	public static void main(String[] args) {
		SpringApplication.run(MeetupClientApplication.class, args);
	}

	@Bean
	public ApplicationRunner initializeConnection(RsvpsWebSocketHandler rsvpsWebSocketHandler) {
		return args -> {
			WebSocketClient rsvpsSocketClient = new StandardWebSocketClient();
			rsvpsSocketClient.doHandshake(rsvpsWebSocketHandler, meetupRsvpsEndpoint);
		};
	}

}
